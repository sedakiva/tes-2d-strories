/****************************************************************************
** Meta object code from reading C++ file 'physics_game_object.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../TES2D/gameLevel/physics_game_object.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'physics_game_object.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PhysicsGameObject_t {
    QByteArrayData data[7];
    char stringdata0[99];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PhysicsGameObject_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PhysicsGameObject_t qt_meta_stringdata_PhysicsGameObject = {
    {
QT_MOC_LITERAL(0, 0, 17), // "PhysicsGameObject"
QT_MOC_LITERAL(1, 18, 18), // "LocationWasChanged"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 18), // "NewArrowWasCreated"
QT_MOC_LITERAL(4, 57, 13), // "arrow::Arrow*"
QT_MOC_LITERAL(5, 71, 15), // "WeaponWasChange"
QT_MOC_LITERAL(6, 87, 11) // "InCollision"

    },
    "PhysicsGameObject\0LocationWasChanged\0"
    "\0NewArrowWasCreated\0arrow::Arrow*\0"
    "WeaponWasChange\0InCollision"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PhysicsGameObject[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x06 /* Public */,
       3,    1,   35,    2, 0x06 /* Public */,
       5,    1,   38,    2, 0x06 /* Public */,
       6,    0,   41,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,

       0        // eod
};

void PhysicsGameObject::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PhysicsGameObject *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->LocationWasChanged(); break;
        case 1: _t->NewArrowWasCreated((*reinterpret_cast< arrow::Arrow*(*)>(_a[1]))); break;
        case 2: _t->WeaponWasChange((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->InCollision(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< arrow::Arrow* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (PhysicsGameObject::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhysicsGameObject::LocationWasChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (PhysicsGameObject::*)(arrow::Arrow * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhysicsGameObject::NewArrowWasCreated)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (PhysicsGameObject::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhysicsGameObject::WeaponWasChange)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (PhysicsGameObject::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhysicsGameObject::InCollision)) {
                *result = 3;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PhysicsGameObject::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_PhysicsGameObject.data,
    qt_meta_data_PhysicsGameObject,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PhysicsGameObject::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PhysicsGameObject::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PhysicsGameObject.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "SimpleGameObject"))
        return static_cast< SimpleGameObject*>(this);
    return QObject::qt_metacast(_clname);
}

int PhysicsGameObject::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void PhysicsGameObject::LocationWasChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void PhysicsGameObject::NewArrowWasCreated(arrow::Arrow * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void PhysicsGameObject::WeaponWasChange(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void PhysicsGameObject::InCollision()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
